import random


# Simple randomized name.
# Uses only two words from each name.
# Creates nonsensical names like 'and Mario.'
def simple_name(midi_split, sounds_split):
    random_midi = midi_split[random.randint(0, len(midi_split) - 1)]
    random_sounds = sounds_split[random.randint(0, len(sounds_split) - 1)]
    random_list = [random_midi, random_sounds]
    random.shuffle(random_list)
    out = ''.join(random_list)
    return out


# This fucntion picks what names will be used and splits each word up to make lists.
# It then randomly picks one of the name combo functions and returns the output.
def name_combo(midi_json, sounds_json):
    # Split the names into individual words.
    midi_split = names[0].split(' ')
    sounds_split = names[1].split(' ')

    # Randomly choose a name function and return the output.
    choices = [simple_name(midi_split, sounds_split)]
    out = random.choice(choices)
    return out
