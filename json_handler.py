import json
import os
import random

# Import project modules.
from names import name_combo


def get_rand_file(folder):
    rand_file = random.choice(os.listdir(folder))
    return rand_file


def get_json():
    midi = get_rand_file('./midi_json/')
    sounds = get_rand_file('./sounds_json/')

    midi_full = './midi_json/' + midi
    sounds_full = './sounds_json/' + sounds

    dir_list_json = [midi_full, sounds_full]
    filename_list_json = [midi, sounds]

    global dir_list_json
    global filename_list_json

    return dir_list_json


def handle_json():

    # Open the midi json file and load it as a dictionary.
    with open(dir_list_json[1]) as f:
        midi_json = json.loads(f)

    # Open the sounds json file and load it as a dictionary.
    with open(dir_list_json[2]) as f:
        sounds_json = json.loads(f)

    # Logic hell and if/else if/else statement hell inbound. This will be highly commentated so that it makes sense.

    # 'notavail' is shorthand for the string 'N/A'
    # This is done because we do not want empty values and cannot have null values in json.
    # This also allows us to assume that mistakes were made in a json file if it contains empty/null values.
    # Though, half the point of the json file is to prevent mistakes; the other half being slight automation.
    # This is set once here globally, but I may move it to some module of it's own.
    notavail = 'N/A'
    global notavail

    # For now the logic here will be very simple, newer logic will be developed in another file and tested...
    # ...as a module until it is finished, at which point it will be moved into this module.

    # DUAL LOGIC #
    # These do not get exported until the logic in this file clears both json files.
    # If for whatever reason one or both fail, then there will be a re-roll for both until a suitable match is found.
    # Eventually this check will include a restriction on repeats, which will be checked using backup files and jsons.
    # The way that is achieved could change, though.

    # Call the name combo function before we get down to buisness.
    # We need a name for our output json file.
    output_name = name_combo(midi_json, sounds_json)
